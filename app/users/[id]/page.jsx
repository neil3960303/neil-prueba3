import { notFound } from "next/navigation";
async function getUser(id) {
  const res = await fetch(`https://reqres.in/api/users/${id}`);
  const data = await res.json();
  return data.data;
}
const UserPage = async ({ params }) => {
  const user = await getUser(params.id);
  if (!user) notFound();
  return (
    <div>
      <h1>User</h1>
      <div>
        <img src={user.avatar} alt="" />
        <h3>
          {user.id} {user.first_name} {user.last_name}
        </h3>
        <p>{user.email}</p>
      </div>
    </div>
  );
};

export default UserPage;
