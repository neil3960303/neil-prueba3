import React from "react";
import Users from "@/components/Users";

async function fetchUsers() {
  const res = await fetch("https://reqres.in/api/users");
  const data = await res.json();
  return data.data;
}

const IndexPage = async () => {
  const users = await fetchUsers();
  return (
    <div>
      <h1>Index Page Home</h1> <Users users={users} />
    </div>
  );
};

export default IndexPage;
